/***************************************************************
 * Name:      mainapp.h
 * Purpose:   Defines Application Class
 * Author:     ()
 * Created:   2017-12-30
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef MAINAPP_H
#define MAINAPP_H

#include <wx/app.h>

class HexlyApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // MAINAPP_H
