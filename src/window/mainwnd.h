/***************************************************************
 * Name:      mainwnd.h
 * Purpose:   Defines Application Frame
 * Author:     ()
 * Created:   2017-12-30
 * Copyright:  ()
 * License:
 **************************************************************/

#ifndef MAINWND_H
#define MAINWND_H

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

class MainFrame: public wxFrame
{
    public:
        MainFrame(wxFrame *frame, const wxString& title);
        ~MainFrame();
    private:
        enum
        {
            idMenuQuit = 1000,
            idMenuAbout
        };
        void OnClose(wxCloseEvent& event);
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        DECLARE_EVENT_TABLE()
};

#endif // MAINWND_H
