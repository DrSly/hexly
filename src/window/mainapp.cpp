/***************************************************************
 * Name:      mainapp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2017-12-30
 * Copyright:  ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "mainapp.h"
#include "mainwnd.h"

// ----------------------------------------------------------------------------
// Main entry point
// ----------------------------------------------------------------------------

// on Windows this translates to extern "C" int WINAPI WinMain(...)
IMPLEMENT_APP(HexlyApp);

bool HexlyApp::OnInit()
{
    MainFrame* frame = new MainFrame(0L, _("Hexly"));
    frame->SetIcon(wxICON(aaaa)); // To Set App Icon
    frame->Show();

    return true;
}
